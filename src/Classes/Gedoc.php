<?php

namespace Freedome\Gedoc\Classes;

use SoapClient;
use SoapFault;
use Exception;

use Freedome\Gedoc\Classes\Auth\LogIn;

use Freedome\Gedoc\Classes\Project\CreateProject;

use Freedome\Gedoc\Classes\ClassificationScheme\GetActiveClassificationScheme;

use Freedome\Gedoc\Classes\AddressBook\AddCorrespondent;
use Freedome\Gedoc\Classes\AddressBook\DeleteCorrespondent;

use Freedome\Gedoc\Classes\Document\CreateDocument;
use Freedome\Gedoc\Classes\Document\GetDocument;
use Freedome\Gedoc\Classes\Document\UploadFileToDocument;
use Freedome\Gedoc\Classes\Document\AddDocInProject;

class Gedoc
{

    private $endpoints;
    private $credentials;
    private $auth_client;
    private $doc_client;
    private $project_client;
    private $schemes_client;
    private $address_client;
    private $auth_request;
    private $project_request;
    private $scheme_request;
    private $address_request;
    private $document_request;

    private $correspondent;

    function __construct($exit_code = 0) 
    {
    	$this->endpoints = [
    		'auth' => config('freedom-gedoc.auth_endpoint'),
    		'doc' => config('freedom-gedoc.doc_endpoint'),
    		'schemes' => config('freedom-gedoc.schemes_endpoint'),
    		'project' => config('freedom-gedoc.project_endpoint'),
    		'address' => config('freedom-gedoc.address_endpoint'),
    	];

    	$this->credentials = (object)[
    		'UserName' => config('freedom-gedoc.UserName'),
    		'Password' => config('freedom-gedoc.Password'),
    		'CodAdm' => config('freedom-gedoc.CodAdm')
    	];

        $this->auth_request = new LogIn($this->credentials->UserName,$this->credentials->CodAdm,$this->credentials->Password);

    	/*Now i set-up the SOAP clients*/

    	try {
            $this->setAuthConnection();
            $this->setSchemesConnection();
	    	$this->setDocConnection();
	    	$this->setProjectConnection();
            $this->setAddressConnection();

            return $exit_code;
        }
        catch(SoapFault $fault){
        	return new \Exception('Something is gone wrong with the initial connection with the endpoints');
        }
    }

    public function getMethod($name)
    {
        try{
            switch ($name) {
                case 'auth':
                    print_r( $this->auth_client->__getFunctions() );
                    print_r( $this->auth_client->__getTypes() );
                    break;
                case 'doc':
                    print_r( $this->doc_client->__getFunctions() );
                    print_r( $this->doc_client->__getTypes() );
                    break;
                case 'schemes':
                    print_r( $this->schemes_client->__getFunctions() );
                    print_r( $this->schemes_client->__getTypes() );
                    break;
                case 'project':
                    print_r( $this->project_client->__getFunctions() );
                    print_r( $this->project_client->__getTypes() );
                    break;
                case 'address':
                    print_r( $this->address_client->__getFunctions() );
                    print_r( $this->address_client->__getTypes() );
                    break;
                default:
                    print_r( 'Nothing to show...' );
                    break;
            }
        }
        catch(SoapFault $fault){
            return new Exception('Something is gone wrong with the initial connection with the endpoint');
        }
    }

    public function setAuthConnection()
    {
    	$options = array( 
            'use' => SOAP_LITERAL,
            'soap_version' => SOAP_1_1,
            'trace' => true,
            'encoding' => 'UTF-8',
            'exceptions' => true,
        );

        try {
            $this->auth_client = new SoapClient($this->endpoints['auth'], $options);
        }
        catch(SoapFault $fault){
        	return new Exception('Something is gone wrong with the initial connection with the endpoint');
        }
    }

    public function setSchemesConnection()
    {
        $options = array( 
            'use' => SOAP_LITERAL,
            'soap_version' => SOAP_1_1,
            'trace' => true,
            'encoding' => 'UTF-8',
            'exceptions' => true,
        );

        try {
            $this->schemes_client = new SoapClient($this->endpoints['schemes'], $options);
        }
        catch(SoapFault $fault){
            return new Exception('Something is gone wrong with the initial connection with the endpoint');
        }
    }

    public function setAddressConnection()
    {
        $options = array( 
            'use' => SOAP_LITERAL,
            'soap_version' => SOAP_1_1,
            'trace' => true,
            'encoding' => 'UTF-8',
            'exceptions' => true,
        );

        try {
            $this->address_client = new SoapClient($this->endpoints['address'], $options);
        }
        catch(SoapFault $fault){
            return new Exception('Something is gone wrong with the initial connection with the endpoint');
        }
    }


    public function setDocConnection()
    {
    	$options = array( 
            'use' => SOAP_LITERAL,
            'soap_version' => SOAP_1_1,
            'trace' => true,
            'encoding' => 'UTF-8',
            'exceptions' => true,
        );

        try {
            $this->doc_client = new SoapClient($this->endpoints['doc'], $options);
        }
        catch(SoapFault $fault){
        	return new Exception('Something is gone wrong with the initial connection with the endpoint');
        }
    }

    public function setProjectConnection()
    {
    	$options = array( 
            'use' => SOAP_LITERAL,
            'soap_version' => SOAP_1_1,
            'trace' => true,
            'encoding' => 'UTF-8',
            'exceptions' => true,
        );

        try {
            $this->project_client = new SoapClient($this->endpoints['project'], $options);
        }
        catch(SoapFault $fault){
        	return new Exception('Something is gone wrong with the initial connection with the endpoint');
        }
    }

    public function login()
    {
        
        try{
            $result = $this->auth_client->LogIn($this->auth_request);
            $this->auth_request->setToken($result->LogInResult->AuthenticationToken);
        }
        catch(SoapFault $fault){
            return new Exception($fault->getMessage());
        }
    }

    public function getActiveClassificationScheme()
    {
        $this->scheme_request = new GetActiveClassificationScheme($this->auth_request->request->UserName,$this->auth_request->request->CodeAdm,$this->auth_request->request->AuthenticationToken);
        try{
            $result = $this->schemes_client->GetActiveClassificationScheme($this->scheme_request);
            return $result->GetActiveClassificationSchemeResult->ClassificationScheme;
        }
        catch(SoapFault $fault){
            return new Exception($fault->getMessage());
        }
    }


    public function CreateDocument()
    {
        $this->document_request = new CreateDocument($this->auth_request->request->UserName,$this->auth_request->request->CodeAdm,$this->auth_request->request->AuthenticationToken);
        try{
            $result = $this->doc_client->CreateDocument($this->document_request);
            return $result->CreateDocumentResult->Document;
        }
        catch(SoapFault $fault){
            return new Exception($fault->getMessage());
        }
    }

    public function GetDocument($document_id)
    {
        $this->document_request = new GetDocument($this->auth_request->request->UserName,$this->auth_request->request->CodeAdm,$this->auth_request->request->AuthenticationToken,$document_id);
        try{
            $result = $this->doc_client->GetDocument($this->document_request);
            return $result->GetDocumentResult->Document;
        }
        catch(SoapFault $fault){
            return new Exception($fault->getMessage());
        }
    }

    public function UploadFileToDocument($document_id, $file, $description)
    {
        $this->document_request = new UploadFileToDocument($this->auth_request->request->UserName,$this->auth_request->request->CodeAdm,$this->auth_request->request->AuthenticationToken,$document_id,$file,$description);

        try{
            $result = $this->doc_client->UploadFileToDocument($this->document_request);
            return $result->UploadFileToDocumentResult;
        }
        catch(SoapFault $fault){
            return new Exception($fault->getMessage());
        }
    }

    public function AddDocInProject($document_id, $project_id)
    {
        $this->document_request = new AddDocInProject($this->auth_request->request->UserName,$this->auth_request->request->CodeAdm,$this->auth_request->request->AuthenticationToken,$document_id, $project_id);

        try{
            $result = $this->doc_client->AddDocInProject($this->document_request);
            return $result->AddDocInProjectResult;
        }
        catch(SoapFault $fault){
            return new Exception($fault->getMessage());
        }
    }

    public function createProject($codNode, $description)
    {
        $class_scheme = $this->getActiveClassificationScheme();
        $this->project_request = new CreateProject($this->auth_request->request->UserName,$this->auth_request->request->CodeAdm,$this->auth_request->request->AuthenticationToken, $class_scheme, $codNode, $description, $this->correspondent);

        try{
            $result = $this->project_client->CreateProject($this->project_request);
            return $result->CreateProjectResult->Project;
        }
        catch(SoapFault $fault){
            return new Exception($fault->getMessage());
        }
    }

    public function addCorrespondent($name, $surname, $type, $desc, $code, $correspondentType, $isCommonAddress, $vatNumber)
    {
        $this->address_request = new AddCorrespondent($this->auth_request->request->UserName,$this->auth_request->request->CodeAdm,$this->auth_request->request->AuthenticationToken, $name, $surname, $type, $desc, $code, $correspondentType, $isCommonAddress, $vatNumber);

        try{
            $result = $this->address_client->AddCorrespondent($this->address_request);
            return $result->AddCorrespondentResult->Correspondent;
        }
        catch(SoapFault $fault){
            return new Exception($fault->getMessage());
        }
    }

    public function deleteCorrespondent($id)
    {
        $this->address_request = new DeleteCorrespondent($this->auth_request->request->UserName,$this->auth_request->request->CodeAdm,$this->auth_request->request->AuthenticationToken, $id);

        try{
            $result = $this->address_client->DeleteCorrespondent($this->address_request);
            return $result;
        }
        catch(SoapFault $fault){
            return new Exception($fault->getMessage());
        }
    }
}
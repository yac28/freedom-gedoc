<?php

namespace Freedom\Gedoc\Classes\General;

class Correspondent
{

    public $AOOCode;
    public $Address;
    public $AdmCode;
    public $Cap;
    public $City;
    public $Code;
    public $CodeRegisterOrRF;
    public $CorrespondentType;
    public $Description;
    public $Email;
    public $Fax;
    public $Id;
    public $IsCommonAddress;
    public $Location;
    public $Name;
    public $Nation;
    public $NationalIdentificationNumber;
    public $Note;
    public $PhoneNumber;
    public $PreferredChannel;
    public $Province;
    public $Surname;
    public $Type;
    public $VatNumber;


    function __construct($name=null, $surname=null, $type=null, $desc=null, $code=null, $correspondentType=null, $isCommonAddress=null, $vatNumber=null,$id=null) 
    {    
    	$this->Name = $name;
       	$this->Surname = $surname;
       	$this->CorrespondentType = $correspondentType;
       	$this->Description = $desc;
       	$this->Code = $code;
        $this->Id = $id;
    }
}
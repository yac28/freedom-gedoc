<?php

namespace Freedom\Gedoc\Classes\General;

class File
{

    public $Content;
    public $Description;
    public $Firmato;
    public $Id;
    public $MimeType;
    public $Name;
    public $VersionId;


    function __construct($file) 
    {    
       	$this->Content = $file->content;
       	$this->Description = 'Complaint File';
       	$this->Firmato = '';
       	$this->Id = '1';
       	$this->MimeType = $file->mime_type;
       	$this->Name = $file->extension;
       	$this->VersionId = '1';
    }
}
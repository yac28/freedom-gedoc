<?php

namespace Freedom\Gedoc\Classes\General;

class ClassificationScheme
{

    public $Active;
 	public $Description;
 	public $Id;


    function __construct($active=null, $description=null, $id=null) 
    { 
    	$this->Active = $active;
    	$this->Description = $description;
    	$this->Id = $id;
    }
}
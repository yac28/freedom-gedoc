<?php

namespace Freedom\Gedoc\Classes\General;

use Freedom\Gedoc\Classes\General\ClassificationScheme;
use Freedom\Gedoc\Classes\General\Correspondent;

class Project
{

    public $ClassificationScheme;
 	public $CodeNodeClassification;
 	public $CollocationDate;
	public $Description;
 	public $Note; 
 	public $Paper;
 	public $PhysicsCollocation;
 	public $Private;
 	public $Template;
 	public $UfficioReferente;

    function __construct($scheme, $codNode, $description, $correspondent) 
    {
    	$this->Paper = false;
    	$this->Private = false;  
    	$this->ClassificationScheme = new ClassificationScheme($scheme->Active,$scheme->Description,$scheme->Id);
    	$this->CodeNodeClassification = $codNode;
    	$this->Description = $description;
        $this->UfficioReferente = new Correspondent(null, null, null, null, '5748910', 'E', null, null,'79090689');
    }
}
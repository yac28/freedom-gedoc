<?php

namespace Freedom\Gedoc\Classes\General;

class Document
{

    public $DocumentType;
 	public $Object;
 	public $Note;
 	public $Template;
 	public $MainDocument;
 	public $Ufficioreferente;


    function __construct() 
    {
    	$this->DocumentType = 'G';
    	$this->Object = 'Complaint File';  
    }
}
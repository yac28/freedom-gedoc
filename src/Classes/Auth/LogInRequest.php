<?php

namespace Freedom\Gedoc\Classes\Auth;

use Freedom\Gedoc\Classes\General\Request;

class LogInRequest extends Request
{

    public $Password;

    function __construct($user, $codamm, $psw) 
    {    
       $this->CodeAdm = $codamm;
       $this->UserName = $user;
       $this->Password = $psw;
    }
}
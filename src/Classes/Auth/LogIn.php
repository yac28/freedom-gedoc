<?php

namespace Freedom\Gedoc\Classes\Auth;

use Freedom\Gedoc\Classes\Auth\LogInRequest;

class LogIn
{

    public $request;

    function __construct($user, $codamm, $psw) 
    {  
    	$this->request = new LogInRequest($user,$codamm,$psw);
    }

    public function setToken($token)
    {
    	$this->request->AuthenticationToken = $token;
    }
}
<?php

namespace Freedom\Gedoc\Classes\ClassificationScheme;

use Freedom\Gedoc\Classes\General\Request;

class GetActiveClassificationSchemeRequest extends Request
{
    function __construct($user, $codamm, $token) 
    {    
       $this->CodeAdm = $codamm;
       $this->UserName = $user;
       $this->AuthenticationToken = $token;
    }
}
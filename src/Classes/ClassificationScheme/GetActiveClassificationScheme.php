<?php

namespace Freedom\Gedoc\Classes\ClassificationScheme;

use Freedom\Gedoc\Classes\ClassificationScheme\GetActiveClassificationSchemeRequest;

class GetActiveClassificationScheme
{

    public $request;

    function __construct($user, $codamm, $token) 
    {  
    	$this->request = new GetActiveClassificationSchemeRequest($user,$codamm,$token);
    }
}
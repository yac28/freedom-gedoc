<?php

namespace Freedom\Gedoc\Classes\AddressBook;

use Freedom\Gedoc\Classes\General\Correspondent;
use Freedom\Gedoc\Classes\General\Request;

class AddCorrespondentRequest extends Request
{

    public $Correspondent;

    function __construct($user, $codamm, $token, $name, $surname, $type, $desc, $code, $correspondentType, $isCommonAddress, $vatNumber) 
    {
    	$this->CodeAdm = $codamm;
       	$this->UserName = $user;
       	$this->AuthenticationToken = $token;
    	$this->Correspondent = new Correspondent($name, $surname, $type, $desc, $code, $correspondentType, $isCommonAddress, $vatNumber);
    }
}
<?php

namespace Freedom\Gedoc\Classes\AddressBook;

use Freedom\Gedoc\Classes\General\Request;

class DeleteCorrespondentRequest extends Request
{

    public $CorrespondentId;

    function __construct($user, $codamm, $token, $id) 
    {
    	$this->CodeAdm = $codamm;
       	$this->UserName = $user;
       	$this->AuthenticationToken = $token;
    	$this->CorrespondentId = $id;
    }
}
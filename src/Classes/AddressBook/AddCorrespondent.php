<?php

namespace Freedom\Gedoc\Classes\AddressBook;

use Freedom\Gedoc\Classes\AddressBook\AddCorrespondentRequest;

class AddCorrespondent
{

    public $request;

    function __construct($user, $codamm, $token, $name, $surname, $type, $desc, $code, $correspondentType, $isCommonAddress, $vatNumber) 
    {  
    	$this->request = new AddCorrespondentRequest($user, $codamm, $token, $name, $surname, $type, $desc, $code, $correspondentType, $isCommonAddress, $vatNumber);
    }
}
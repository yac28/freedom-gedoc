<?php

namespace Freedom\Gedoc\Classes\AddressBook;

use Freedom\Gedoc\Classes\AddressBook\DeleteCorrespondentRequest;

class DeleteCorrespondent
{

    public $request;

    function __construct($user, $codamm, $token, $id) 
    {
    	$this->request = new DeleteCorrespondentRequest($user, $codamm, $token, $id);
    }
}
<?php

namespace Freedom\Gedoc\Classes\Project;

use Freedom\Gedoc\Classes\Project\CreateProjectRequest;

class CreateProject
{

    public $request;

    function __construct($user, $codamm, $token, $scheme, $codNode, $description, $correspondent) 
    {  
    	$this->request = new CreateProjectRequest($user, $codamm, $token, $scheme, $codNode, $description, $correspondent);
    }
}
<?php

namespace Freedom\Gedoc\Classes\Project;

use Freedom\Gedoc\Classes\General\Request;
use Freedom\Gedoc\Classes\General\Project;

class CreateProjectRequest extends Request
{

    public $Project;

    function __construct($user, $codamm, $token, $scheme, $codNode, $description, $correspondent) 
    {  
    	$this->CodeAdm = $codamm;
       	$this->UserName = $user;
       	$this->AuthenticationToken = $token;
    	$this->Project = new Project($scheme, $codNode, $description, $correspondent);
    }
}
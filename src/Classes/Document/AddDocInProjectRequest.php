<?php

namespace Freedom\Gedoc\Classes\Document;

use Freedom\Gedoc\Classes\General\Request;

class AddDocInProjectRequest extends Request
{

    public $IdDocument;
    public $IdProject;
    public $CodeProject;

    function __construct($user, $codamm, $token, $idDoc, $idPrj) 
    {  
    	$this->CodeAdm = $codamm;
       	$this->UserName = $user;
       	$this->AuthenticationToken = $token;
    	$this->IdDocument = $idDoc;
    	$this->IdProject = $idPrj;
    }
}
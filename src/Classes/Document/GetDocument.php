<?php

namespace Freedom\Gedoc\Classes\Document;

use Freedom\Gedoc\Classes\Document\GetDocumentRequest;

class GetDocument
{

    public $request;

    function __construct($user, $codamm, $token, $id) 
    {  
    	$this->request = new GetDocumentRequest($user, $codamm, $token, $id);
    }
}
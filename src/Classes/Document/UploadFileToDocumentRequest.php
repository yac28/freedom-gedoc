<?php

namespace Freedom\Gedoc\Classes\Document;

use Freedom\Gedoc\Classes\General\Request;
use Freedom\Gedoc\Classes\General\File;

class UploadFileToDocumentRequest extends Request
{

    public $AttachmentType;
    public $CovertToPDFA;
    public $CreateAttachment;
    public $Description;
    public $File;
    public $HashFile;
    public $IdDocument;

    function __construct($user, $codamm, $token, $id, $file, $desc) 
    {  
    	$this->CodeAdm = $codamm;
       	$this->UserName = $user;
       	$this->AuthenticationToken = $token;
    	$this->IdDocument = $id;
        $this->File = new File($file);
        $this->Description = $desc;
    }
}
<?php

namespace Freedom\Gedoc\Classes\Document;

use Freedom\Gedoc\Classes\General\Request;
use Freedom\Gedoc\Classes\General\Document;

class CreateDocumentRequest extends Request
{

    public $Document;
    public $CodeRegister;
    public $CodeRF;

    function __construct($user, $codamm, $token) 
    {  
    	$this->CodeAdm = $codamm;
       	$this->UserName = $user;
       	$this->AuthenticationToken = $token;
    	$this->Document = new Document();
    }
}
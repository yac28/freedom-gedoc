<?php

namespace Freedom\Gedoc\Classes\Document;

use Freedom\Gedoc\Classes\Document\CreateDocumentRequest;

class CreateDocument
{

    public $request;

    function __construct($user, $codamm, $token) 
    {  
    	$this->request = new CreateDocumentRequest($user, $codamm, $token);
    }
}
<?php

namespace Freedom\Gedoc\Classes\Document;

use Freedom\Gedoc\Classes\Document\AddDocInProjectRequest;

class AddDocInProject
{

    public $request;

    function __construct($user, $codamm, $token, $idDoc, $idPrj) 
    {  
    	$this->request = new AddDocInProjectRequest($user, $codamm, $token, $idDoc, $idPrj);
    }
}
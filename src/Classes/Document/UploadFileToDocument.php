<?php

namespace Freedom\Gedoc\Classes\Document;

use Freedom\Gedoc\Classes\Document\UploadFileToDocumentRequest;

class UploadFileToDocument
{

    public $request;
    

    function __construct($user, $codamm, $token, $id, $file, $desc) 
    {  
    	$this->request = new UploadFileToDocumentRequest($user, $codamm, $token, $id, $file, $desc);
    }
}
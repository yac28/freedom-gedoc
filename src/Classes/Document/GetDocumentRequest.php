<?php

namespace Freedom\Gedoc\Classes\Document;

use Freedom\Gedoc\Classes\General\Request;

class GetDocumentRequest extends Request
{

    public $GetFile;
    public $GetFileWithSignature;
    public $IdDocument;
    public $Signature;

    function __construct($user, $codamm, $token, $id) 
    {  
    	$this->CodeAdm = $codamm;
       	$this->UserName = $user;
       	$this->AuthenticationToken = $token;
    	$this->IdDocument = $id;
        $this->GetFile = true;
    }
}
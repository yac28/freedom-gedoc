<?php

namespace Freedome\Gedoc;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\AliasLoader;

class GedocServiceProvider extends ServiceProvider
{
    /**
     *
     */
    public function register()
    {
        $this->app->singleton('Gedoc', function () {
            return new Freedom\Gedoc\Classes\Gedoc;
        });
        
        $this->mergeConfigFrom(
            __DIR__.'/publishable/freedom-gedoc.php', 'freedom-gedoc'
        );
    }
    
    /**
     *
     */
    public function boot()
    {
        $this->publishes([__DIR__.'/publishable/freedom-gedoc.php' => config_path('freedom-gedoc.php')], 'config');
    }

}
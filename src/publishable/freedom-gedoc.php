<?php

return [

    'UserName' => env('GEDOC_USERNAME','test1'),
    'Password' => env('GEDOC_PASSWORD','password**'),
    'CodAdm' => env('GEDOC_CODADM','ENAC'),

    'auth_endpoint' => env('GEDOC_AUTH_ENDPOINT','http://192.168.101.134/docspa5esercizio-be/VtDocsWS/WebServices/Authentication.svc?wsdl'),
    'doc_endpoint' => env('GEDOC_DOC_ENDPOINT','http://192.168.101.134/docspa5esercizio-be/VtDocsWS/WebServices/Documents.svc?wsdl'),
    'project_endpoint' => env('GEDOC_PRJ_ENDPOINT','http://192.168.101.134/docspa5esercizio-be/VtDocsWS/WebServices/Projects.svc?wsdl'),
    'schemes_endpoint' => env('GEDOC_SCHEMES_ENDPOINT','http://192.168.101.134/docspa5esercizio-be/VtDocsWS/WebServices/ClassificationSchemes.svc?wsdl'),
    'address_endpoint' => env('GEDOC_ADDRESS_ENDPOINT','http://192.168.101.134/docspa5esercizio-be/VtDocsWS/WebServices/AddressBook.svc?wsdl'),
    
];
